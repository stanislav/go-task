# Maintainer: George Rawlinson <grawlinson@archlinux.org>

pkgname=go-task
pkgver=3.37.2
pkgrel=1
pkgdesc='A task runner written in Go'
arch=('x86_64')
url='https://taskfile.dev'
license=('MIT')
depends=('glibc')
makedepends=('git' 'go')
options=('!lto')
install=go-task.install
source=(
  "$pkgname::git+https://github.com/go-task/task#tag=v$pkgver"
  'rename-completions.patch'
)
sha512sums=('feb0c2a4e588a3f91d98833b9f4780f34121fbdf352114fd9077ba655eb2a91bd4ee95daa5a3a2cbca0ac1d4e5dc394ef4e03159fd38d25d3f7ac83c2edf47dd'
            'd2958f196ddd641c8efff06d303b646e14bbfd79dcbb57288b08d8f0c85a75b11d9835f895a061186eafabd873d68d0b9e80471220d6843b85d9cbdc99511e2c')
b2sums=('70916d31160fa419df2302d76e956992fe0b8fadc3c3a46c511ba202933f50fcd1b43ae59b963ccd0ab76d96fb5290212483f2465e2f539a226d6cfc3c2f0ba6'
        '75a539c6cbec03eb71fda9ba2a26d16ab32656c0703f53df41b4ac8572710df72c76a82a788355152600a9cddcca11382268bd8c991e07ff8b0decfc09b16025')

prepare() {
  cd "$pkgname"

  # download dependencies
  export GOPATH="${srcdir}"
  go mod download

  # rename completions from `task` to `go-task`
  patch -p1 -i "$srcdir/rename-completions.patch"
}

build() {
  cd "$pkgname"

  # set Go flags
  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  export GOPATH="${srcdir}"

  go build -v \
    -buildmode=pie \
    -mod=readonly \
    -modcacherw \
    -ldflags "-compressdwarf=false \
    -linkmode external \
    -extldflags '${LDFLAGS}' \
    -X github.com/go-task/task/v3/internal/version.version=${pkgver}" \
    -o go-task \
    ./cmd/task
}

check() {
  cd "$pkgname"

  go test -v ./...
}

package() {
  cd "$pkgname"

  # binary
  install -vDm755 -t "$pkgdir/usr/bin" go-task

  # shell completions
  install -vDm644 completion/bash/task.bash "$pkgdir/usr/share/bash-completion/completions/go-task"
  install -vDm644 completion/fish/task.fish "$pkgdir/usr/share/fish/vendor_completions.d/go-task.fish"
  install -vDm644 completion/zsh/_task "$pkgdir/usr/share/zsh/site-functions/_go-task"

  # license
  install -vDm644 -t "$pkgdir/usr/share/licenses/$pkgname" LICENSE
}
